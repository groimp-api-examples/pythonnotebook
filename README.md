# GroPy-GroLink example Notebook


The Notebook in this repository shows the basic functionality of the [GroPy library](https://gitlab.com/grogra/groimp-utils/pythonapilibrary) and the[ GroLink API](https://wiki.grogra.de/doku.php?id=user-guide:additional_interfaces:api). 

**It is important to mention that this notebook can only work if GroIMP is running as an API server and the server is reachable for the notebook. 
Running the notebook in a environment like Google Colab will not work (or only with a lot of preparation).**

## Requirements

The pipeline requires a running instance of the GroLink api, instructions can be found [here](https://wiki.grogra.de/doku.php?id=tutorials:startup-api) and alternatively a docker image with all needed dependencies [here](https://gitlab.com/groimp-api-examples/docker).
Additionally the GroPy library must be installed. The installation of GroPy is explained [here](https://gitlab.com/grogra/groimp-utils/pythonapilibrary#installation).

